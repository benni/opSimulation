################################################################################
# Copyright (c) 2021 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
#
# This program and the accompanying materials are made available under the
# terms of the Eclipse Public License 2.0 which is available at
# http://www.eclipse.org/legal/epl-2.0.
#
# SPDX-License-Identifier: EPL-2.0
################################################################################
#
# Find Package Adapter for OSI (Open Simulation Interface)
#
# Creates the follwoing imported targets (if available):
# - osi::shared
# - osi::static
# - osi::pic

set(OSI_SHARED_NAMES
  open_simulation_interface.lib
  libopen_simulation_interface.dll.a
  libopen_simulation_interface.so
)

set(OSI_STATIC_NAMES
  open_simulation_interface_static.lib
  libopen_simulation_interface_static.a
)

set(OSI_PIC_NAMES
  open_simulation_interface_pic.lib
  libopen_simulation_interface_pic.a
)

find_library(OSI_SHARED_LIBRARY NAMES ${OSI_SHARED_NAMES}
  PATHS
    ${PREFIX_PATH}
    /usr/local
    /usr
  PATH_SUFFIXES
    lib/osi3
    lib
    lib64
)

find_library(OSI_STATIC_LIBRARY NAMES ${OSI_STATIC_NAMES}
  PATHS
    ${PREFIX_PATH}
    /usr/local
    /usr
  PATH_SUFFIXES
    lib/osi3
    lib
    lib64
)

find_library(OSI_PIC_LIBRARY NAMES ${OSI_PIC_NAMES}
  PATHS
    ${PREFIX_PATH}
    /usr/local
    /usr
  PATH_SUFFIXES
    lib/osi3
    lib
    lib64
)

if(OSI_SHARED_LIBRARY)
  message(STATUS "Found OSI (shared): ${OSI_SHARED_LIBRARY}")
  
  get_filename_component(OSI_SHARED_LIBRARY_DIR "${OSI_SHARED_LIBRARY}" DIRECTORY)

  add_library(osi::shared IMPORTED SHARED)
  set_target_properties(osi::shared
                        PROPERTIES
                          IMPORTED_LOCATION ${OSI_SHARED_LIBRARY}
                          IMPORTED_IMPLIB ${OSI_SHARED_LIBRARY}
                          INTERFACE_INCLUDE_DIRECTORIES ${OSI_SHARED_LIBRARY_DIR}/../../include
                          INTERFACE_LINK_LIBRARIES protobuf::libprotobuf)
else()
  message(STATUS "Didn't find OSI (shared)")
endif()

if(OSI_STATIC_LIBRARY)
  message(STATUS "Found OSI (static): ${OSI_STATIC_LIBRARY}")
  get_filename_component(OSI_STATIC_LIBRARY_DIR "${OSI_STATIC_LIBRARY}" DIRECTORY)
  add_library(osi::static IMPORTED STATIC)
  set_target_properties(osi::static
                        PROPERTIES
                          IMPORTED_LOCATION ${OSI_STATIC_LIBRARY}
                          INTERFACE_INCLUDE_DIRECTORIES ${OSI_STATIC_LIBRARY_DIR}/../../include
                          INTERFACE_LINK_LIBRARIES protobuf::libprotobuf_static)
else()
  message(STATUS "Didn't find OSI (static)")
endif()

if(OSI_PIC_LIBRARY)
  message(STATUS "Found OSI (pic): ${OSI_PIC_LIBRARY}")
  get_filename_component(OSI_PIC_LIBRARY_DIR "${OSI_PIC_LIBRARY}" DIRECTORY)
  add_library(osi::pic IMPORTED STATIC)
  set_target_properties(osi::pic
                        PROPERTIES
                          IMPORTED_LOCATION ${OSI_PIC_LIBRARY}
                          INTERFACE_INCLUDE_DIRECTORIES ${OSI_PIC_LIBRARY_DIR}/../../include
                          INTERFACE_LINK_LIBRARIES protobuf::libprotobuf_static)
else()
  message(STATUS "Didn't find OSI (pic)")
endif()


unset(OSI_SHARED_LIBRARY)
unset(OSI_STATIC_LIBRARY)
unset(OSI_PIC_LIBRARY)
